<?php

// [SECTION] Objects as Variable
// Step by step approach
// Using the procedural approach.
$buildingObj = (object)[
	'name' =>'Caswynn Building',
	'floors' => 8,
	'address' => 'Brgy. Sacred Heart, Quezon City, Philippines'
];

// [SECTION] Objects from Classes
// hides the logic within the object and focuses more on the data structure within the program.

// Created a class building
class Building{

	// Properties
	public $name;
	public $floors;
	public $address;

	// The constructor method is automatically executed when the "new Building" or "new Condominium" is used. 
	public function __construct($name, $floors, $address){
		 /*
            $this - points you to the object you are currently working in.
                  - it is usually used because we do not know what variable object will be assigned to.
            $this->name = $name - accessing the property with the name of the current object and assigning the value of $name upon instantiation.
        */
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	// Method
	public function printName(){
		return "The name of the building is $this->name";
	}
}
// Instantiate the Building class to create a new building object.
$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');

// [SECTION] Inheritance and Polymorphism

// Inheritance - The derived classes are allowed to inherit variables and methods from a specified base class.
	// The "extends" keyword is used to derive a class from another class.
	// A derived class (child) has all the properties and methods of the base class (parent).
class Condominium extends Building{

	// Polymorphism - Methods inherited by a derived class that can be overriden to have a behavior different from the method of base class.
		// The printName of this class overrides the behavior of the printName from the Building class.
	public function printName(){
		return "The name of the condominium is $this->name";
	}
}

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');

// Building is the parent class, while condominium is the child class.